import bpy

# Propriété dépendante de la Scene Blender, moyen trouvé pour stocker le résultat du Test et le réutiliser dans d'autres fonctions
bpy.types.Scene.resultat_perso_test = bpy.props.StringProperty()
bpy.context.scene.resultat_perso_test = ""

# Opérateur uniquement là pour afficher une petite fenêtre donnant le résultat du test une fois fait
class DisplayPersonalityTestResult(bpy.types.Operator):
    """Affiche le résultat du test de personalité"""
    bl_label = "Résultat de votre test"
    bl_idname = "wm.display_personality_test_result"
    bl_options = {"INTERNAL"}  # Avant était REGISTER : https://docs.blender.org/api/current/bpy_types_enum_items/operator_type_flag_items.html
    
    def draw(self, context):
        layout = self.layout
        col = layout.column()
        row = layout.row()
        
        resultat = bpy.context.scene.resultat_perso_test
        
        # Dictionnaire avec dedans des infos utiles au bon affichage du nom et icônes des objets
        object_utils = {
            'empty': {
                'icon' : 'EMPTY_AXIS',
                'name' : 'un Empty',
            },
            'cube': {
                'icon' : 'MESH_CUBE',
                'name' : 'un Cube',
            },
            'plane': {
                'icon' : 'MESH_PLANE',
                'name' : 'un Plane',
            },
            'uv_sphere': {
                'icon' : 'MESH_UVSPHERE',
                'name' : 'une Sphere',
            },
            'circle': {
                'icon' : 'MESH_CIRCLE',
                'name' : 'un Cercle',
            },
            'icosphere': {
                'icon' : 'MESH_ICOSPHERE',
                'name' : 'une Icosphere',
            },
            'cylinder': {
                'icon' : 'MESH_CYLINDER',
                'name' : 'un Cylindre',
            },
            'cone': {
                'icon' : 'MESH_CONE',
                'name' : 'un Cone',
            },
            'torus': {
                'icon' : 'MESH_TORUS',
                'name' : 'un Torus',
            },
            'monkey': {
                'icon' : 'MESH_MONKEY',
                'name' : 'une Suzanne',
            },
            'grid': {
                'icon' : 'MESH_GRID',
                'name' : 'une Grid',
            },
        }
        
        if bpy.context.scene.resultat_perso_test:
            row.label(text=("Vous êtes " + object_utils[resultat]['name'] + " !"), icon = object_utils[resultat]['icon'])
            row = layout.row()
        else:
            # Si jamais l'utilisateur n'a pas encore fait le test mais lance quand même l'opérateur par un autre moyen
            row.label(text=("Hé ! Faites le test d'abord ! >:("))
    
    def execute(self, context):
        return {'FINISHED'}
    
    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)
        
        

# Opérateur qui lance le test de personalité
class PersonalityTest(bpy.types.Operator):
    """Découvrez quel objet vous êtes"""
    bl_label = "Test de personalité"
    bl_idname = "wm.personality_test"
    bl_options = {"REGISTER"}  # "UNDO" enlevé pour ne pas qu'une fenêtre s'affiche à la fin du test
    
    # Liste avec les intitulés de chaque question
    questions = [
        "Comment décrierais-tu ton lieu de vie ? (chambre)",
        "Social ?",
        "Comment tu exprimes ta colère ?",
        "Ton style vestimentaire ?",
        "Tes émotions ?",
        "Des idées d'avenir ?",
        "Ce que tu aimes faire ?",
        "En quoi tu es doué ?",
    ]
    
    # Listes avec les réponses items des enums des réponses possibles pour chaque question
    
    # Déclaration item de la liste, dans l'ordre :
    # ( 'Valeur' , 'Nom', 'Description' )
    
    q1_answers = [
        ('R1', "Rien.", "Reponse 1"),  # Empty
        ('R2', "Classique", "Reponse 2"),  # Cube
        ('R3', "Vide, comme mon âme", "Reponse 3"),  # Plane
        ('R4', "Ma bulle à moi", "Reponse 4"),  # UV Sphere
        ('R5', "C'est un lieu sacré", "Reponse 5"),  # Circle
        ('R6', "Atypique", "Reponse 6"),  # Icosphere
        ('R7', "Style Louis XVI", "Reponse 7"),  # Cylinder
        ('R8', "Un bordel organisé", "Reponse 8"),  # Cone
        ('R9', "C'est un terrain dangereux", "Reponse 9"),  # Torus
        ('R10', "Rangement ? ça se mange ?", "Reponse 10"),  # Suzanne
        ('R11', "J'ai mis des étiquettes de partout", "Reponse 11"),  # Grid
    ]
    
    q2_answers = [
        ('R1', "Non.", "Reponse 1"),  # Empty
        ('R2', "50/50", "Reponse 2"),  # Cube
        ('R3', "Les gens ? *vomis*", "Reponse 3"),  # Plane
        ('R4', "STRESSANT.", "Reponse 4"),  # UV Sphere
        ('R5', "Toujours connaitre ses ennemis...", "Reponse 5"),  # Circle
        ('R6', "J'ai 100000 amis !!", "Reponse 6"),  # Icosphere
        ('R7', "Je reste cordial et civilisé", "Reponse 7"),  # Cylinder
        ('R8', "J'aime toute le monde ! (sauf lui là bas au fond)", "Reponse 8"),  # Cone
        ('R9', "Maladroit. bcp trop", "Reponse 9"),  # Torus
        ('R10', "P'tite bière ????", "Reponse 10"),  # Suzanne
        ('R11', "Toujours entre 16h et 19h", "Reponse 11"),  # Grid
    ]
    
    q3_answers = [
        ('R1', "Je ne fais rien.", "Reponse 1"),  # Empty
        ('R2', "Je le dis !!", "Reponse 2"),  # Cube
        ('R3', "Ma vie n'est que colère", "Reponse 3"),  # Plane
        ('R4', "Je pleure...", "Reponse 4"),  # UV Sphere
        ('R5', "Je connais ta mère alors tu vas te calmer tout de suite Timothé", "Reponse 5"),  # Circle
        ('R6', "JAMAIS!", "Reponse 6"),  # Icosphere
        ('R7', "Je garde mon sang froid", "Reponse 7"),  # Cylinder
        ('R8', "Je fais une story visée", "Reponse 8"),  # Cone
        ('R9', "Je hurle de manière compulsive", "Reponse 9"),  # Torus
        ('R10', "Je gifle / tape", "Reponse 10"),  # Suzanne
        ('R11', "Avec 3 sucres dans mon café au lieu de 2", "Reponse 11"),  # Grid
    ]
    
    q4_answers = [
        ('R1', "Rien.", "Reponse 1"),  # Empty
        ('R2', "T-shirt et jean !", "Reponse 2"),  # Cube
        ('R3', "Gros t-shirt de métal", "Reponse 3"),  # Plane
        ('R4', "Robe !", "Reponse 4"),  # UV Sphere
        ('R5', "Tant qu'on ne m'identifie pas...", "Reponse 5"),  # Circle
        ('R6', "Toujours EXTRA!", "Reponse 6"),  # Icosphere
        ('R7', "Costard, cravate", "Reponse 7"),  # Cylinder
        ('R8', "Toujours à la mode", "Reponse 8"),  # Cone
        ('R9', "...j'ai pas fait de machine...", "Reponse 9"),  # Torus
        ('R10', "Claquette/chaussette, toujours à l'aise !", "Reponse 10"),  # Suzanne
        ('R11', "ça m'arrive de sortir avec des chausettes différentes... la honte...", "Reponse 11"),  # Grid
    ]
    
    q5_answers = [
        ('R1', "Inexistantes", "Reponse 1"),  # Empty
        ('R2', "Des plus classique", "Reponse 2"),  # Cube
        ('R3', "Je ne connais pas la joie", "Reponse 3"),  # Plane
        ('R4', "OMG UN PAPILLON !", "Reponse 4"),  # UV Sphere
        ('R5', "Je préfère ne pas en parler", "Reponse 5"),  # Circle
        ('R6', "Toujours joyeuse", "Reponse 6"),  # Icosphere
        ('R7', "Enfin, restons serieux", "Reponse 7"),  # Cylinder
        ('R8', "Aigrie", "Reponse 8"),  # Cone
        ('R9', "Je ne les comprend pas", "Reponse 9"),  # Torus
        ('R10', "Emotions explosives", "Reponse 10"),  # Suzanne
        ('R11', "Modérée", "Reponse 11"),  # Grid
    ]
    
    q6_answers = [
        ('R1', "Aucune", "Reponse 1"),  # Empty
        ('R2', "Faire le tour du monde ! ", "Reponse 2"),  # Cube
        ('R3', "Le futur n'existe pas...", "Reponse 3"),  # Plane
        ('R4', "Avoir une licorne ^^", "Reponse 4"),  # UV Sphere
        ('R5', "Régner sur la civilisation ou un truc du genre", "Reponse 5"),  # Circle
        ('R6', "Avoir une nouvelle garde robe", "Reponse 6"),  # Icosphere
        ('R7', "Investir pour gagner plus d'argent", "Reponse 7"),  # Cylinder
        ('R8', "Faire une giga fête!!", "Reponse 8"),  # Cone
        ('R9', "Avoir la derniere console !!! ", "Reponse 9"),  # Torus
        ('R10', "Une autre ptite bière", "Reponse 10"),  # Suzanne
        ('R11', "Rentrer chez moi", "Reponse 11"),  # Grid
    ]
    
    q7_answers = [
        ('R1', "Rien.", "Reponse 1"),  # Empty
        ('R2', "Netflix", "Reponse 2"),  # Cube
        ('R3', "Pleurer", "Reponse 3"),  # Plane
        ('R4', "Me promener", "Reponse 4"),  # UV Sphere
        ('R5', "Faire mes petits rituels", "Reponse 5"),  # Circle
        ('R6', "Me regarder", "Reponse 6"),  # Icosphere
        ('R7', "Répondre aux mails", "Reponse 7"),  # Cylinder
        ('R8', "Dormir", "Reponse 8"),  # Cone
        ('R9', "Dessiner", "Reponse 9"),  # Torus
        ('R10', "Parler avec les copains", "Reponse 10"),  # Suzanne
        ('R11', "Nettoyer sous mon frigo", "Reponse 11"),  # Grid
    ]
    
    q8_answers = [
        ('R1', "En rien.", "Reponse 1"),  # Empty
        ('R2', "TO BE ADDED", "Reponse 2"),  # Cube
        ('R3', "Me plaindre", "Reponse 3"),  # Plane
        ('R4', "Ecouter les autres", "Reponse 4"),  # UV Sphere
        ('R5', "Sacrifier... des choses !", "Reponse 5"),  # Circle
        ('R6', "En tout !!!", "Reponse 6"),  # Icosphere
        ('R7', "Convaincre les gens", "Reponse 7"),  # Cylinder
        ('R8', "Fortnite", "Reponse 8"),  # Cone
        ('R9', "Faire rire les autres", "Reponse 9"),  # Torus
        ('R10', "Soulever des trucs et je parle pas que de ta maman hehe", "Reponse 10"),  # Suzanne
        ('R11', "Plier mes linges", "Reponse 11"),  # Grid
    ]
    
    # Fonction pour créer une question, évite répétition de toute la déclaration de propriété
    def create_question(numero_q, items_q):
        return bpy.props.EnumProperty(
            name='Q'+str(numero_q),  # Nom de la propriété, réutilisée pour l'appeller plus loins
            description="Question "+str(numero_q),
            items= items_q
        )
    
    # Declaration des questions
    Q1: create_question(1, q1_answers)
    
    Q2: create_question(2, q2_answers)
    
    Q3: create_question(3, q3_answers)
    
    Q4: create_question(4, q4_answers)
    
    Q5: create_question(5, q5_answers)
    
    Q6: create_question(6, q6_answers)
    
    Q7: create_question(7, q7_answers)
    
    Q8: create_question(8, q8_answers)

    # Fenêtre du test
    def draw(self, context):
        layout = self.layout
        col = layout.column()
        row = layout.row()
        
        col.label(text='Découvrez quel objet de Blender vous êtes grâce à ce test !')
        col.label(text='Pour chaque question choisissez la réponse que parmi les menus déroulants')
        
        # Crée 2 lignes pour chaque question et son menu de réponses
        for q in range(len(self.questions)):
            # Affichage des questions et leur liste de réponses
            # La question dans text=""
            col.label(text=str(self.questions[q]))
            # La liste des réponses
            col.prop(self, "Q"+str(q+1), expand=False, text="")
    
    # Calcul des points et résultat du test
    def execute(self, context):
        # Chaque réponse donne des points dans tel ou tel objets, des fois beaucoup, des fois peu, celui qui a le plus de points à la fin
        # est celui qui sera le résultat
        
        # Dictionnaire pour stocker les réponses et y itérer plus facilement
        user_answers = {
            'Rep_Q1': self.Q1,
            'Rep_Q2': self.Q2,
            'Rep_Q3': self.Q3,
            'Rep_Q4': self.Q4,
            'Rep_Q5': self.Q5,
            'Rep_Q6': self.Q6,
            'Rep_Q7': self.Q7,
            'Rep_Q8': self.Q8
        }
        
        # Score par réponse pour chaque question
        question_answers_scores = {
            # Comment décrierais-tu ton lieu de vie ? (chambre)
            'Q1' : {'R1' : { 'empty': 1},
                    'R2' : { 'cube': 1},
                    'R3' : { 'plane': 1},
                    'R4' : { 'uv_sphere': 1, 'icosphere': .5},
                    'R5' : { 'circle': 1},
                    'R6' : { 'icosphere': 1, 'uv_sphere': .5},
                    'R7' : { 'cylinder': 1},
                    'R8' : { 'cone': 1, 'monkey': .1},
                    'R9' : { 'torus': 1},
                    'R10' : { 'monkey': 1},
                    'R11' : { 'grid': 1}
                },
            # Social ?
            'Q2' : {'R1' : { 'empty': 1},
                    'R2' : { 'cube': 1},
                    'R3' : { 'plane': 1, 'uv_sphere': .25, 'circle': .25},
                    'R4' : { 'uv_sphere': 1, 'torus': .25},
                    'R5' : { 'circle': 1},
                    'R6' : { 'icosphere': 1},
                    'R7' : { 'cylinder': 1, 'circle': .1},
                    'R8' : { 'cone': 1},
                    'R9' : { 'torus': 1, 'uv_sphere': 25},
                    'R10' : { 'monkey': 1},
                    'R11' : { 'grid': 1}
                },
            # Comment tu exprimes ta colère ?
            'Q3' : {'R1' : { 'empty': 1},
                    'R2' : { 'cube': 1},
                    'R3' : { 'plane': 1},
                    'R4' : { 'uv_sphere': 1, 'plane': .5},
                    'R5' : { 'circle': 1},
                    'R6' : { 'icosphere': 1},
                    'R7' : { 'cylinder': 1},
                    'R8' : { 'cone': 1},
                    'R9' : { 'torus': 1},
                    'R10' : { 'monkey': 1},
                    'R11' : { 'grid': 1}
                },
            # Ton style vestimentaire ?
            'Q4' : {'R1' : { 'empty': 1},
                    'R2' : { 'cube': 1, 'monkey': .1},
                    'R3' : { 'plane': 1},
                    'R4' : { 'uv_sphere': 1},
                    'R5' : { 'circle': 1},
                    'R6' : { 'icosphere': 1},
                    'R7' : { 'cylinder': 1, 'grid': .25},
                    'R8' : { 'cone': 1},
                    'R9' : { 'torus': 1},
                    'R10' : { 'monkey': 1},
                    'R11' : { 'grid': 1}
                },
            # Tes émotions ?
            'Q5' : {'R1' : { 'empty': 1, 'plane': .1},
                    'R2' : { 'cube': 1},
                    'R3' : { 'plane': 1},
                    'R4' : { 'uv_sphere': 1, 'icosphere': .5},
                    'R5' : { 'circle': 1, 'plane': .1},
                    'R6' : { 'icosphere': 1},
                    'R7' : { 'cylinder': 1, 'grid': .5},
                    'R8' : { 'cone': 1, 'plane': .5},
                    'R9' : { 'torus': 1},
                    'R10' : { 'monkey': 1},
                    'R11' : { 'grid': 1}
                },
            # Des idées d'avenir ?
            'Q6' : {'R1' : { 'empty': 1},
                    'R2' : { 'cube': 1},
                    'R3' : { 'plane': 1, 'circle': .25},
                    'R4' : { 'uv_sphere': 1},
                    'R5' : { 'circle': 1},
                    'R6' : { 'icosphere': 1},
                    'R7' : { 'cylinder': 1},
                    'R8' : { 'cone': 1, 'monkey' : .5},
                    'R9' : { 'torus': 1},
                    'R10' : { 'monkey': 1},
                    'R11' : { 'grid': 1}
                },
            # Ce que tu aimes faire ?
            'Q7' : {'R1' : { 'empty': 1},
                    'R2' : { 'cube': 1},
                    'R3' : { 'plane': 1},
                    'R4' : { 'uv_sphere': 1},
                    'R5' : { 'circle': 1},
                    'R6' : { 'icosphere': 1, 'cone': .25},
                    'R7' : { 'cylinder': 1},
                    'R8' : { 'cone': 1},
                    'R9' : { 'torus': 1, 'uv_sphere': .5},
                    'R10' : { 'monkey': 1},
                    'R11' : { 'grid': 1}
                },
            # En quoi tu es doué ?
            'Q8' : {'R1' : { 'empty': 1},
                    'R2' : { 'cube': 1},
                    'R3' : { 'plane': 1},
                    'R4' : { 'uv_sphere': 1},
                    'R5' : { 'circle': 1},
                    'R6' : { 'icosphere': 1},
                    'R7' : { 'cylinder': 1},
                    'R8' : { 'cone': 1, 'monkey': .25},
                    'R9' : { 'torus': 1, 'monkey': .1},
                    'R10' : { 'monkey': 1},
                    'R11' : { 'grid': 1}
                }
        }
        
        # Dictionnaire contenant le score pour chaque objet
        final_score = {
            'empty': 0,
            'cube': 0,
            'plane': 0,
            'uv_sphere': 0,
            'circle': 0,
            'icosphere': 0,
            'cylinder': 0,
            'cone': 0,
            'torus': 0,
            'monkey': 0,
            'grid': 0
        }
        
        # Operateurs à utiliser en fonction du score final
        final_score_operators = {
            'empty': bpy.ops.object.empty_add,
            'cube': bpy.ops.mesh.primitive_cube_add,
            'plane': bpy.ops.mesh.primitive_plane_add,
            'uv_sphere': bpy.ops.mesh.primitive_uv_sphere_add,
            'circle': bpy.ops.mesh.primitive_circle_add,
            'icosphere': bpy.ops.mesh.primitive_ico_sphere_add,
            'cylinder': bpy.ops.mesh.primitive_cylinder_add,
            'cone': bpy.ops.mesh.primitive_cone_add,
            'torus': bpy.ops.mesh.primitive_torus_add,
            'monkey': bpy.ops.mesh.primitive_monkey_add,
            'grid': bpy.ops.mesh.primitive_grid_add
        }
        
        # Calcul des points
        for q, answer in zip(question_answers_scores, user_answers):
            for score in question_answers_scores[q][user_answers[answer]]:
                final_score[score] += question_answers_scores[q][user_answers[answer]][score]

        # Comment ça c'est pas limpide comme code ? Je me souviens parfaitement de pourquoi il marche ! Il euh... euh, itère dans ?... euh...
        
        # L'objet qui a le plus gros score
        resultat = max(final_score, key=final_score.get)
        # Stocker dans la variable de scène
        bpy.context.scene.resultat_perso_test = resultat
        
        final_score_operators[resultat]()
        
        # Affiche une fenêtre avec le résultat
        bpy.ops.wm.display_personality_test_result('INVOKE_DEFAULT')
        
        return {'FINISHED'}

    # Affiche de ma fenêtre de l'opérateur
    def invoke(self, context, event):
        
        return context.window_manager.invoke_props_dialog(self, width = 410)


# Opérateur qui "invoque" le test de personalité (pré-test), passe par 2 opérateur afin d'avoir la bonne fenêtre, une fenêtre pop-up
# flottante et non une fenêtre en bas à gauche comme pour les opérateurs d'objets
class InvokePersonalityTest(bpy.types.Operator):
    """Découvrez quel objet vous êtes"""
    bl_label = "Personality Test"
    bl_idname = "mesh.invoke_test"
    bl_options = {"INTERNAL"}  # Avant "REGISTER", "UNDO"
    
    def execute(self, context):
        # 'INVOKE_DEFAULT' permet d'invoquer la fenêtre, autrement ne se fait pas
        bpy.ops.wm.personality_test('INVOKE_DEFAULT')
        return {'FINISHED'}


# Sous menu dans le menu Add
def menu_add(self, context):
    self.layout.operator("mesh.invoke_test", text="Test de personalité", icon = "SHADERFX")


# Panneau latéral
class VIEW3D_PT_personality_test_panel(bpy.types.Panel):
    """Votre test de personalité"""
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Test de personalité"
    bl_category = "Test de personalité"
    bl_idname = "MCN_PT_KB_AL"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        texte_op = ""
        
        row = self.layout.row()
        
        resultat = bpy.context.scene.resultat_perso_test
        
        # Dictionnaire avec dedans des infos utiles au bon affichage du nom et icônes des objets
        object_utils = {
            'empty': {
                'icon' : 'EMPTY_AXIS',
                'name' : 'un Empty',
                'description' : 'Le Cube est un blablabla'
            },
            'cube': {
                'icon' : 'MESH_CUBE',
                'name' : 'un Cube',
                'description' : 'Le Cube est un blablabla'
            },
            'plane': {
                'icon' : 'MESH_PLANE',
                'name' : 'un Plane',
                'description' : 'Le Cube est un blablabla'
            },
            'uv_sphere': {
                'icon' : 'MESH_UVSPHERE',
                'name' : 'une Sphere',
                'description' : 'Le Cube est un blablabla'
            },
            'circle': {
                'icon' : 'MESH_CIRCLE',
                'name' : 'un Cercle',
                'description' : 'Le Cube est un blablabla'
            },
            'icosphere': {
                'icon' : 'MESH_ICOSPHERE',
                'name' : 'une Icosphere',
                'description' : 'Le Cube est un blablabla'
            },
            'cylinder': {
                'icon' : 'MESH_CYLINDER',
                'name' : 'un Cylindre',
                'description' : 'Le Cube est un blablabla'
            },
            'cone': {
                'icon' : 'MESH_CONE',
                'name' : 'un Cone',
                'description' : 'Le Cube est un blablabla'
            },
            'torus': {
                'icon' : 'MESH_TORUS',
                'name' : 'un Torus',
                'description' : 'Le Cube est un blablabla'
            },
            'monkey': {
                'icon' : 'MESH_MONKEY',
                'name' : 'une Suzanne',
                'description' : 'Le Cube est un blablabla'
            },
            'grid': {
                'icon' : 'MESH_GRID',
                'name' : 'une Grid',
                'description' : 'Le Cube est un blablabla'
            },
        }
        
        if bpy.context.scene.resultat_perso_test:
            texte_op = "Refaire le test !"
        else:
            texte_op = "Faire le test !"
        
        if bpy.context.scene.resultat_perso_test:
            row.label(text=("Vous êtes " + object_utils[resultat]['name'] + " !"), icon=object_utils[resultat]['icon'])
        else:
            row.label(text=("Découvrez quel objet vous êtes !"))
        row = self.layout.row()
        row.operator("wm.personality_test", text=texte_op, icon="SHADERFX")

# Liste des classes pour le register()
classes = [DisplayPersonalityTestResult, PersonalityTest, InvokePersonalityTest, VIEW3D_PT_personality_test_panel]

# Registration
def register():
    
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.VIEW3D_MT_add.prepend(menu_add)

# Unregistration
def unregister():
    
    for cls in classes:
        bpy.utils.unregister_class(cls)
    
    bpy.types.VIEW3D_MT_add.remove(menu_add)

# Beaucoup de texte, beaucoup de lignes, surtout pour contenir le texte des questions et des réponses et des liens entre elles
