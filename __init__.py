bl_info = {
    "name": "Master Création Numérique 2023-2024",
    "author": "Various Artists",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "description": "Various tools (TBD)",
    "warning": "",
    "doc_url": "",
    "category": "Misc",
}


import bpy
from . import (CF_EG, CV_AO, GM_SL, IN_AP, KB_AL, MG_SB, NM_AM, SP_VA, TT_QG)


class MCN_PT_Main_Panel(bpy.types.Panel):
    """"""
    bl_label = "Master Création Numérique"
    bl_idname = "MCN_PT_Main_Panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Various tools")


def register():
    bpy.utils.register_class(MCN_PT_Main_Panel)
    for module in (CF_EG, CV_AO, GM_SL, IN_AP, KB_AL, MG_SB, NM_AM, SP_VA, TT_QG):
        module.register()


def unregister():
    for module in (CF_EG, CV_AO, GM_SL, IN_AP, KB_AL, MG_SB, NM_AM, SP_VA, TT_QG):
        module.unregister()
    bpy.utils.unregister_class(MCN_PT_Main_Panel)


if __name__ == "__main__":
    register()
